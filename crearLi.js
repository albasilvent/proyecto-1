"use strict";

export function pegarImagenes(
    parametroElectrodomesticos,
    parametroHoraActual,
    parametroPrecioConsumo
) {
    const ul = document.querySelector(".ulMain");
    let fragment = document.createDocumentFragment();
    for (let i = 0; i < parametroElectrodomesticos.length; i++) {
        let li = document.createElement("li");
        li.innerHTML = `
    <h2>${parametroElectrodomesticos[i].aparato}</h2>
    <p>${parametroElectrodomesticos[i].consumo}W</p>
    <img alt="${parametroElectrodomesticos[i].aparato}" src="./multimedia/SVG/${
            parametroElectrodomesticos[i].aparato
        }.svg"/>
    <p>Desde ${
        parametroHoraActual === 1 ? "la" : "las"
    } ${parametroHoraActual}:00 hasta ${
            parametroHoraActual === 1 ? "la" : "las"
        } ${parametroHoraActual + 1}:00 el precio es de ${
            parametroPrecioConsumo[i]
        } cent/h.</p>
    `;
        fragment.append(li);
    }
    ul.append(fragment);
}
