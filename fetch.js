"use strict";

export async function getDatos(parametroHora, parametroPrecio) {
    //Funcion para sacar los datos de la API

    // let infoLocalStorage = JSON.parse(localStorage.getItem("datos"));
    // console.log(infoLocalStorage);

    let infoLocalStorage = null;

    if (
        localStorage.getItem("datos") &&
        Date.now() - JSON.parse(localStorage.getItem("datos")).fetchTime <
            300000
    ) {
        const { contenido } = JSON.parse(localStorage.getItem("datos"));

        //Creamos un array de los valores, luego creamos dos arrays vacios para ir metiendo la informacion por separado del objeto
        let valores = Object.values(contenido);

        for (let i = 0; i < valores.length; i++) {
            parametroHora.push(valores[i].hour);
            parametroPrecio.push(valores[i].price);
        }
    } else {
        // const results = await fetch(
        //     "https://api.allorigins.win/get?url=https://api.preciodelaluz.org/v1/prices/all?zone=PCB"
        // );

        const results = await fetch("datos.json"); // Medida temporal hasta que se arregle problema CORS

        const datos = await results.json();

        // let { contents: contenido } = datos; Para cuando se hace el fetch con la URL completa

        // Contents era un JSON, asi que lo pasamos a objeto
        // let contenido = JSON.parse(contents);
        let contenido = datos; // Medida temporal hasta que se arregle problema CORS

        //Hacemos el fetchTime
        const fetchTime = new Date().getTime();

        //Contruimos un objeto o mandarlo directamente al LocalStorage
        localStorage.setItem("datos", JSON.stringify({ fetchTime, contenido }));

        //Creamos un array de los valores, luego creamos dos arrays vacios para ir metiendo la informacion por separado del objeto
        let valores = Object.values(contenido);
        for (let i = 0; i < valores.length; i++) {
            parametroHora.push(valores[i].hour);
            parametroPrecio.push(valores[i].price);
        }
    }
}
