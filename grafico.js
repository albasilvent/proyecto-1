"use strict";

export function pintarGrafico(parametroHora, parametroPrecio) {
    let precioAlto = Math.max(...parametroPrecio);
    let precioBajo = Math.min(...parametroPrecio);
    let grafico = document.querySelector(".grafico");
    const fragment = document.createDocumentFragment();
    for (let i = 0; i < parametroHora.length; i++) {
        const heightLi =
            parametroPrecio[i] === precioAlto
                ? 100
                : (parametroPrecio[i] * 100) / precioAlto;
        let colorLi = null;
        if (parametroPrecio[i] === precioAlto) {
            colorLi = "red";
        } else if (parametroPrecio[i] === precioBajo) {
            colorLi = "green";
        } else {
            colorLi = "orange";
        }

        const li = document.createElement("li");
        li.style.cssText = `width:20%; height:${heightLi}%; background-color:${colorLi}`;
        fragment.append(li);
    }
    grafico.append(fragment);
}
