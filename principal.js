"use strict";

// Hacemos el import
import { getDatos } from "./fetch.js";
import { enlazarHora } from "./enlaceHora.js";
import { operacion } from "./operacionPrecio.js";
import { tablaPrecios } from "./ModificarDom.js";
import { pegarImagenes } from "./crearLi.js";
import { pintarGrafico } from "./grafico.js";

//Variables de datos
let precio = [];
let hora = [];

let precioParaFuncion = [];

const electrodomesticos = [
    {
        aparato: "Lavadora",
        consumo: 2000,
    },
    {
        aparato: "Horno",
        consumo: 1800,
    },
    {
        aparato: "Microondas",
        consumo: 800,
    },
    {
        aparato: "Televisor",
        consumo: 250,
    },
    {
        aparato: "Tostadora",
        consumo: 700,
    },
    {
        aparato: "Cafetera",
        consumo: 900,
    },
    {
        aparato: "Aspiradora",
        consumo: 1200,
    },
    {
        aparato: "Frigorifico",
        consumo: 800,
    },
];
let precioConsumo = []; 

//Variables de horas
let fecha = new Date();
// let fechaMilisegundos = new Date().getTime(); //Para el if del LocalStorage
let horaActual = fecha.getHours();
// let intervaloHora = `${horaActual}-${horaActual + 1}`;

//Funciones
await getDatos(hora, precio); //Para hacer el fetch
enlazarHora(hora, precioParaFuncion, horaActual, precio); //Enlazamos el array de las horas con la hora actual para sacar el precio actual y hacer con él las operaciones
operacion(electrodomesticos, precioConsumo, precioParaFuncion); //Hacemos el calculo de precio de cada electrodomestico
// actualizarPrecios(horaActual, precioConsumo); //Modificamos el DOM para añadir el precio a los parrafos
tablaPrecios(precio, hora); //Modificamos el DOM para añadir la hora mas cara y barata
pegarImagenes(electrodomesticos, horaActual, precioConsumo);
pintarGrafico(hora, precio);
