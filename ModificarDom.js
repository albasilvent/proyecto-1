"use strict";

export function tablaPrecios(parametroPrecio, parametroHora) {
    let precioAlto = Math.max(...parametroPrecio);
    let precioBajo = Math.min(...parametroPrecio);

    let horaAlta = [];
    let horaBaja = [];

    for (let i = 0; i < parametroPrecio.length; i++) {
        if (precioAlto === parametroPrecio[i]) {
            horaAlta.push(parametroHora[i]);
        }

        if (precioBajo === parametroPrecio[i]) {
            horaBaja.push(parametroHora[i]);
        }
    }

    let tablaCara = document.querySelector(".horaCara");
    tablaCara.innerHTML = `<p class="tablaCara">Intervalo más caro: ${horaAlta}, con un precio de: <span class="rojo">${precioAlto}€</span> por Mw/h</p>`;

    let parrafoTablaCara = document.querySelector(".tablaCara");
    let spanRojo = document.querySelector(".rojo");
    spanRojo.style = `color: red`;

    let tablaBarata = document.querySelector(".horaBarata");
    tablaBarata.innerHTML = `<p class="tablaBarata">Intervalo más barato: ${horaBaja}, con un precio de: <span class="verde">${precioBajo}€</span> por Mw/h</p>`;

    let parrafoTablaBarata = document.querySelector(".tablaBarata");
    let spanVerde = document.querySelector(".verde");
    spanVerde.style = `color: green`;
}
