"use strict";

export function operacion(
    arrayElectrodomesticos,
    arrayPrecioConsumo,
    parametroPrecioFuncion
) {
    for (let i = 0; i < arrayElectrodomesticos.length; i++) {
        let precioFinal = Math.ceil(
            (arrayElectrodomesticos[i].consumo / 10000) * parametroPrecioFuncion
        ); //Ceil para que no pueda dar 0 cent
        arrayPrecioConsumo.push(precioFinal);
    }
}
